//
//  SBaseManager.h
//  SomeSDK
//
//  Created by Captain Black on 2023/2/27.
//

#import <Foundation/Foundation.h>
#import "SBaseService.h"

NS_ASSUME_NONNULL_BEGIN

BSServiceBind(SBaseService, SBaseManager, 2)
@interface SBaseManager : NSObject <SBaseService>

@end

NS_ASSUME_NONNULL_END
