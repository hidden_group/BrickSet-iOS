//
//  SBaseManager.m
//  SomeSDK
//
//  Created by Captain Black on 2023/2/27.
//

#import "SBaseManager.h"
#import "SomeSDK.h"

@implementation SBaseManager

@end

__attribute__((constructor))
static void loadServices(void) {
    [BrickSet loadStaticBindingServicesByAddress:loadServices];
}
