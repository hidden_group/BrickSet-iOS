//
//  SBaseService.h
//  SomeSDK
//
//  Created by Captain Black on 2023/2/27.
//

#import <Foundation/Foundation.h>
#import <BrickSet/BrickSet.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SBaseService <BSService>

@end

NS_ASSUME_NONNULL_END
