//
//  SomeSDK.h
//  SomeSDK
//
//  Created by Captain Black on 2023/2/27.
//

#import <Foundation/Foundation.h>

//! Project version number for SomeSDK.
FOUNDATION_EXPORT double SomeSDKVersionNumber;

//! Project version string for SomeSDK.
FOUNDATION_EXPORT const unsigned char SomeSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SomeSDK/PublicHeader.h>


#import "SBaseService.h"
