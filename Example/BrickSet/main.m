//
//  main.m
//  BrickSet
//
//  Created by captainblack.soul@gmail.com on 03/17/2017.
//  Copyright (c) 2017 captainblack.soul@gmail.com. All rights reserved.
//

@import UIKit;
#import "BSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSAppDelegate class]));
    }
}
