//
//  BSAppDelegate.h
//  BrickSet
//
//  Created by captainblack.soul@gmail.com on 03/17/2017.
//  Copyright (c) 2017 captainblack.soul@gmail.com. All rights reserved.
//

@import UIKit;

@interface BSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
