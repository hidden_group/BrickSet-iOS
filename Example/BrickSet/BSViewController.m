//
//  BSViewController.m
//  BrickSet
//
//  Created by captainblack.soul@gmail.com on 03/17/2017.
//  Copyright (c) 2017 captainblack.soul@gmail.com. All rights reserved.
//

#import "BSViewController.h"
#import <BrickSet/BrickSet.h>

@interface BSViewController ()

@end

@implementation BSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


@protocol BSMyService <BSService>

@property(nonatomic, strong) NSString* name;
@property(nonatomic, assign) int value;

@end

BSServiceBind(BSMyService, BSMyImp, 2)
@interface BSMyImp : NSObject <BSMyService>
@end
@implementation BSMyImp
@synthesize name;
@synthesize value;

BSServiceInit(BSMyImp) {
    NSLog(@"服务对象初始化");
}

@end
