#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "BrickSet.h"
#import "BrickSetServiceManager.h"
#import "BSService.h"

FOUNDATION_EXPORT double BrickSetVersionNumber;
FOUNDATION_EXPORT const unsigned char BrickSetVersionString[];

