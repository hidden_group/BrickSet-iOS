//
//  BSServiceManager.h
//  HappyRoom
//
//  Created by Captain Black on 2016/11/29.
//  Copyright © 2016年 Captain Black. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BrickSet.h"

@interface BrickSetServiceManager : NSObject

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)sharedInstance;

- (id<BSService>)instanceForService:(Protocol *)serviceProtocol;

- (void)registerClass:(Class)class forService:(Protocol *)serviceProtocol;

- (void)unregisterService:(Protocol *)serviceProtocol;

@end
