//
//  BSService.h
//  BrickSet
//
//  Created by Captain Black on 2017/3/10.
//  Copyright © 2017年 captainblack. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BSService <NSObject>

@end

// 声明协议的初始化方法，BrickSet在创建该协议的实例时会尝试调用该方法初始化
#define BSServiceInit(_name_)   \
- (void)init##_name_
