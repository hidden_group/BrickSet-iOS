# BrickSet

[![CI Status](http://img.shields.io/travis/captainblack.soul@gmail.com/BrickSet.svg?style=flat)](https://travis-ci.org/captainblack.soul@gmail.com/BrickSet)
[![Version](https://img.shields.io/cocoapods/v/BrickSet.svg?style=flat)](http://cocoapods.org/pods/BrickSet)
[![License](https://img.shields.io/cocoapods/l/BrickSet.svg?style=flat)](http://cocoapods.org/pods/BrickSet)
[![Platform](https://img.shields.io/cocoapods/p/BrickSet.svg?style=flat)](http://cocoapods.org/pods/BrickSet)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BrickSet is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "BrickSet"
```

## Author

captainblack.soul@gmail.com, captainblack.soul@gmail.com

## License

BrickSet is available under the MIT license. See the LICENSE file for more info.
